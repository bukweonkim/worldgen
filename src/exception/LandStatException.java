/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exception;

/**
 *
 * @author Administrator
 */
public class LandStatException extends Exception {

    /**
     * Creates a new instance of <code>LandStatException</code> without detail
     * message.
     */
    public LandStatException() {
    }

    /**
     * Constructs an instance of <code>LandStatException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public LandStatException(String msg) {
        super(msg);
    }
}

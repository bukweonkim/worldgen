/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exception;

/**
 *
 * @author Administrator
 */
public class UnitStatException extends Exception {

    /**
     * Creates a new instance of <code>UnitStatException</code> without detail
     * message.
     */
    public UnitStatException() {
    }

    /**
     * Constructs an instance of <code>UnitStatException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public UnitStatException(String msg) {
        super(msg);
    }
}

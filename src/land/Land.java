/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package land;

import exception.LandStatException;
import units.Unit;
import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class Land {

    int[] reference;
    int[] resource;
    int[] resourceSource;
    ArrayList<Unit> inhabitance;
    Land[] neighborings;

    public Land() {
        this.reference = new int[]{0, 0, 0};
        this.resource = new int[]{0, 0, 0, 0, 0, 0};
        this.resourceSource = new int[]{0, 0, 0, 0, 0, 0};
        this.inhabitance = new ArrayList<Unit>();
        this.neighborings = new Land[4];
    }
    public Land(int face, int row, int column, int fireSource, int waterSource, int windSource, int earthSource, int lightSource, int darknessSource) {
        this.reference = new int[]{face, row, column};
        this.resource = new int[]{0, 0, 0, 0, 0, 0};
        this.resourceSource = new int[]{fireSource, waterSource, windSource, earthSource, lightSource, darknessSource};
        this.inhabitance = new ArrayList<Unit>();
        this.neighborings = new Land[4];
    }
    public int[] getReferenceClone() {
        return reference.clone();
    }
    public int getReference(int i) {
        return reference[i];
    }
    public int[] getResourceClone() {
        return resource.clone();
    }
    public int getResource(int i) {
        return resource[i];
    }
    public int[] getResourceSourceClone() {
        return resourceSource.clone();
    }
    public int getResourceSource(int i) {
        return resourceSource[i];
    }
    public ArrayList<Unit> getInhabitance() {
        return inhabitance;
    }
    public Land getNeighborings(int i) {
        return neighborings[i];
    }
    public void setResource(int i, int resource) throws LandStatException {
        if (resource < 0) {
            throw new LandStatException("resource cannot be negative");
        } else if (i < 0 || i > 5) {
            throw new LandStatException("wrong index of resource");
        } else {
            this.resource[i] = resource;
        }
    }
    public void setResourceSource(int i, int resourceSource) throws LandStatException {
        if (resourceSource < 0) {
            throw new LandStatException("resource source cannot be negative");
        } else if (i < 0 || i > 5) {
            throw new LandStatException("wrong index of resource source");
        } else {
            this.resourceSource[i] = resourceSource;
        }
    }
    public void setNeighborings(int i, Land neighborings) throws LandStatException {
        if (i < 0 || i > 3) {
            throw new LandStatException("wrong index of neighborings");
        } else {
            this.neighborings[i] = neighborings;
            switch (i) {
                case 0:
                case 1:
                    if (neighborings.getNeighborings(i + 2).equals(this)) {
                        break;
                    } else {
                        neighborings.setNeighborings(i + 2, this);
                    }
                    break;
                case 2:
                case 3:
                    if (neighborings.getNeighborings(i - 2).equals(this)) {
                        break;
                    } else {
                        neighborings.setNeighborings(i - 2, this);
                    }
                    break;
            }

        }

    }
}



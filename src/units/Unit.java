/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package units;

import exception.UnitStatException;

/**
 *
 * @author Administrator
 */
public class Unit {
    /*
    Basic stats of unit.
    */
    /*
    Level dependant stats
    */
    private int hpMax;
    private int lvl;
    private int exp;
    /*
    Situational stat
    */
    private int hpCur;
    /*
    4 stats
    */
    private int str;
    private int def;
    private int dex;
    private int spd;
    
    /*
    unchanging stat
    */
    private int mag;
    
    /*
    Hidden stats
    */
    private int agg;
    private int fire;
    private int water;
    private int wind;
    private int earth;
    private int light;
    private int darkness;
    
    public Unit() {
        this.hpMax=0;
        this.lvl=0;
        this.exp=0;
        this.str = 0;
        this.def = 0;
        this.dex = 0;
        this.spd = 0;
        this.mag = 0;
        this.agg = 0;
        this.fire = 0;
        this.water = 0;
        this.wind = 0;
        this.earth = 0;
        this.light = 0;
        this.darkness = 0;
        
        this.hpCur=this.hpMax;
    }    
    public Unit(int str, int def, int dex, int spd, int mag, int agg, int fire, int water, int wind, int earth, int light, int darkness) {
        this.hpMax=10;
        this.lvl=1;
        this.exp=0;
        this.str = str;
        this.def = def;
        this.dex = dex;
        this.spd = spd;
        this.mag = mag;
        this.agg = agg;
        this.fire = fire;
        this.water = water;
        this.wind = wind;
        this.earth = earth;
        this.light = light;
        this.darkness = darkness;
        
        this.hpCur=this.hpMax;
    }
    public int getHpMax() {
        return hpMax;
    }
    public int getLvl() {
        return lvl;
    }
    public int getExp() {
        return exp;
    }
    public int getHpCur() {
        return hpCur;
    }    
    public int getStr() {
        return str;
    }
    public int getDef() {
        return def;
    }
    public int getDex() {
        return dex;
    }
    public int getSpd() {
        return spd;
    }
    public int getMag() {
        return mag;
    }
    public int getAgg() {
        return agg;
    }
    public int getFire() {
        return fire;
    }
    public int getWater() {
        return water;
    }
    public int getWind() {
        return wind;
    }
    public int getEarth() {
        return earth;
    }
    public int getLight() {
        return light;
    }
    public int getDarkness() {
        return darkness;
    }

    public void setHpMax(int hpMax) {
        this.hpMax = hpMax;
    }    
    public void setHpCur(int hpCur) {
        this.hpCur = hpCur;
    }    
    public void setMag(int mag) {
        this.mag = mag;
    }
    public void setAgg(int agg) {
        this.agg = agg;
    }    
    public void setFire(int fire) {
        this.fire = fire;
    }
    public void setWater(int water) {
        this.water = water;
    }
    public void setWind(int wind) {
        this.wind = wind;
    }
    public void setEarth(int earth) {
        this.earth = earth;
    }
    public void setLight(int light) {
        this.light = light;
    }
    public void setDarkness(int darkness) {
        this.darkness = darkness;
    }
    
    public void addExp(int exp) throws UnitStatException{
        if ((this.exp+exp)<0){
            throw new UnitStatException("Cannot lower the exp below 0");
        }else{
            this.exp=this.exp+exp;
        }        
    }
    public void lvlUp(){
        this.lvl=this.lvl+1;
    }
    public void lvlDown() throws UnitStatException{
        if (this.lvl==0){
            throw new UnitStatException("Cannot lower the level below 0");
        }else{
        this.lvl=this.lvl-1;
        }
    }
    
    


    
    
}

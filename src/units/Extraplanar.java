/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package units;

/**
 *
 * @author Administrator
 */
public class Extraplanar extends Unit{
    private int exp_next;
    private int exp_pre;
    private int exp_give;

    public Extraplanar() {
        super();
        exp_next=0;
        exp_pre=0;
        exp_give=0;
    }

    public Extraplanar(int str, int def, int dex, int spd, int agg, int fire, int water, int wind, int earth, int light, int darkness) {
        super(str, def, dex, spd, 10, agg, fire, water, wind, earth, light, darkness);
        exp_next=super.getLvl()*(super.getLvl()+1)*10;
        exp_pre=super.getLvl()*(super.getLvl()-1)*10;
        exp_give=super.getLvl()*4;
    }
   
}
